
#import math

from math import*

MIN_DISTANCE = 100
MAX_DISTANCE = 2000
STEP_DISTANCE = 100
ACCEL_F16 = 7
ACCEL_FORMULA1 = 9
ACCEL_MOTOGP = 10
KMH_TO_MTSEC = (1000.0 / 3600.0)
VMAX_F16 = 2400 * KMH_TO_MTSEC
VMAX_FORMULA1 = 380 * KMH_TO_MTSEC
VMAX_MOTOGP = 350 * KMH_TO_MTSEC
#type
#   type_racer = (F16, FORMULA1, MOTOGP) se pone de la forma:

F16 = 0
FORMULA = 1
MOTOGP = 2



#(****************       procedures and functions      ***************************)

def get_race_distance(min_dist,  max_dist, step_dist ): 
    distance_ok = False
    while (distance_ok != true): #TAmbién se podría poner == False
        print('Please enter race distance: ')
        distance = int(input())
        distance_ok = (distance >= min_dist) and (distance <= max_dist) and (distance % step_dist == 0)
        if (distance_ok == False):
             print('ERROR: Invalid distance!')
    return distance

def get_race_time(race_distance, accel, vmax):
    dist_crosspoint = (vmax * vmax) / (2 * accel) #{ MRUA: v*v = 2*a*s }
    if (race_distance <= dist_crosspoint):
        #{ MRUA only     s = (1/2)*a*t*t  -> t = sqrt(2*s/a) }
        race_time = sqrt(2 * race_distance / accel)
    else:
        #{ MRUA }
        race_time = sqrt(2 * dist_crosspoint / accel)

       # { MRU s = v * t -> t = s / v }
        race_time = race_time + (race_distance - dist_crosspoint) / vmax
  
    return race_time

def print_race_times(t_f16, t_formula1, t_motogp):
    print('Time F16: ', t_f16, ' sec')
    print('Time F1: ', t_formula1, ' sec')
    print('Time motoGP: ', t_motogp, ' sec')


def get_race_winner(t_f16, t_formula1, t_motogp):

    if ((t_f16 < t_formula1) and (t_f16 < t_motogp)):
       t_winner = t_f16
       winner_id = F16
    elif ((t_formula1 < t_f16) and (t_formula1 < t_motogp)):
       t_winner = t_formula1
       winner_id = FORMULA1
    else:
        t_winner = t_motogp
        winner_id = MOTOGP
        
    return t_winner, winner_id


def print_winner(t_winner,  winner_id ):

    if (winner_id == F16):
           print('Winner: F16 (', t_winner, ' sec)')
        
    elif (winner_id == FORMULA1):
            print('Winner: Formula1 (', t_winner, ' sec)')
    elif (winner_id == MOTOGP):  
            print('Winner: motoGP (', t_winner, ' sec)')
    else
        print('Winner: UNKNOWN')



#(********)
#(* main *)
#(********)

#No hace falta ponerlo así por lo que se quita todo 
#var
#    i: integer    
#    race_distance: integer
#   f16_race_time: real
#   formula1_race_time: real
#   motogp_race_time: real
#   t_winner: real
#   winner_id: type_racer

#begin (se quita)

for i in range(3):
    race_distance = get_race_distance(MIN_DISTANCE, MAX_DISTANCE, STEP_DISTANCE)
    f16_race_time = get_race_time(race_distance, ACCEL_F16, VMAX_F16)
    formula1_race_time = get_race_time(race_distance, ACCEL_FORMULA1, VMAX_FORMULA1)
    motogp_race_time = get_race_time(race_distance, ACCEL_MOTOGP, VMAX_MOTOGP)

    print_race_times(f16_race_time, formula1_race_time, motogp_race_time)
    t_winner, winner_id = get_race_winner(f16_race_time, formula1_race_time, motogp_race_time)
    print_winner(t_winner, winner_id)


#end.


