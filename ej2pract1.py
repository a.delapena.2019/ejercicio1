import sys
import math

entero1 = 5
entero2 = 1
listaenteros = [1,8,6,9]

def mayorentero(x, y):
    if (x > y):
        result = x
    else:
        result = y
    return result
def mayorlista1(listaenteros):
    if (listaenteros[1-1] > listaenteros[2-1]):
        result = listaenteros[1-1]
    else:
        result = listaenteros[2-1]
    return result
def mayorlista2(listaenteros):
    if (listaenteros[3-1] > listaenteros[4-1]):
        result = listaenteros[3-1]
    else:
        result = listaenteros[4-1]
    return result
def mayorlistatot(mayor1, mayor2):
    if (mayor1 > mayor2):
        result = mayor1
    else:
        result = mayor2
    return result

print(mayorentero(entero1, entero2))
mayor1 = mayorlista1(listaenteros)
print('El mayor número entre el primer elemento y el segundo es',mayor1)
mayor2 = mayorlista2(listaenteros)
print('El mayor número entre el tercer elemento y el cuarto es', mayor2)
mayorlista = mayorlistatot(mayor1, mayor2)
print('El mayor número de la lista es', mayorlista)

        