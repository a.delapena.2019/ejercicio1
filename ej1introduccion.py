import sys
import math

entero = 2
real = 5.456923
cadena = 'Ana de la Peña Baños'
asignaturas = ['Programación','Ing Operación Aeropuertos', 'Optimización', 'Edificios e Intalaciones']

print(len(cadena)) #Imprime la longitud de la cadena
print(len(asignaturas)) #Imprime el número de elementos de la lista 
print(asignaturas[3-1]) #Muestra el tercer elemento de la lista
print(cadena[3-1])

